---
# Transfer Entropy Neural Estimation -- Python Code

## Juan F. Restrepo and Ramiro Casal

### <jrestrepo@ingenieria.uner.edu.ar>, <ramirocasalmighetto@gmail.com>

_Laboratorio de Señales y Dinámicas no Lineales, Instituto de Bioingeniería y Bioinformática, CONICET - Universidad
Nacional de Entre Ríos. Ruta prov 11 km 10 Oro Verde, Entre Ríos, Argentina._
---

## Abstract

TENE Calculates the transfer entropy rate between two systems using two time series.

### Instructions

### Usage

tentropyrate_lzc(x, y, m, tau, K, alpha) Calculates de transfer entropy rate
between two systems using the time series x and y.

#### Inputs

- x -- Time series from system X
- y -- Time series from system Y
- m -- Embedding-dimension / history-length
- tau -- Embedding delay - time-delay
- K -- Number of surrogate realizations
- alpha -- Number of symbols in the alphabet

#### Outputs

- T -- Global transfer entropy rate T = tyx - txy - (tyxs - txys)
- tyx -- Transfer entropy rate from system Y to system X
- txy -- Transfer entropy rate from system X to system Y
- tyxs -- Surrogate Transfer entropy rate from system Y to system X
- txys -- Surrogate Transfer entropy rate from system X to system Y

### Files

1. tene.py
2. dinamicalsystems.py
3. tene_tools.py

---

## Citing Information

---

2020-12-05

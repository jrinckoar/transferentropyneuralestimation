#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''
Tene : Transfer Entropy Neural Estimation
'''
import torch
import torch.nn as nn
import torch.optim as opt
import math
import numpy as np
from tqdm import tqdm
import matplotlib.pyplot as plt
from mine_tools import tene_batch as batch
from mine_tools import embedding
from dinamical_systems import henonC


if torch.cuda.is_available():
    device = torch.device('cuda')
else:
    device = torch.device('cpu')

device = torch.device('cpu')


class EMALoss(torch.autograd.Function):
    @staticmethod
    def forward(ctx, tensor, running_ema):
        # ctx is a context object that can be used to stash information
        # for backward computation
        ctx.tensor = tensor
        ctx.running_ema = running_ema

        return tensor.exp().mean().log()

    @staticmethod
    def backward(ctx, grad_output):
        grad = grad_output * ctx.tensor.exp().detach() / (ctx.running_ema + 1e-6) / \
            ctx.tensor.shape[0]
        # We return as many input gradients as there were arguments.
        # Gradients of non-Tensor arguments to forward must be None.
        return grad, None


def ema(mu, alpha, past_ema):
    return alpha * mu + (1.0 - alpha) * past_ema


def ema_loss(x, running_mean, alpha):
    t_exp = torch.exp(torch.logsumexp(x, 0) - math.log(x.shape[0])).detach()
    if running_mean == 0:
        running_mean = t_exp
    else:
        running_mean = ema(t_exp, alpha, running_mean.item())
    t_log = EMALoss.apply(x, running_mean)

    # Recalculate ema
    return t_log, running_mean


class Tene(nn.Module):
    def __init__(self, d_in, loss='mine', alpha=0.1, dim_feedforward=100):

        super().__init__()

        self.running_mean1 = 0
        self.running_mean2 = 0
        self.loss = loss
        self.alpha = alpha
        self.d_in = d_in
        self.mi1 = 0
        self.mi2 = 0
        self.te = 0
        self.T1 = nn.Sequential(
            nn.Linear(3 * d_in, dim_feedforward),
            nn.ReLU(),
            nn.Linear(dim_feedforward, dim_feedforward),
            nn.ReLU(),
            nn.Linear(dim_feedforward, 1)
        )
        self.T2 = nn.Sequential(
            nn.Linear(2 * d_in, dim_feedforward),
            nn.ReLU(),
            nn.Linear(dim_feedforward, dim_feedforward),
            nn.ReLU(),
            nn.Linear(dim_feedforward, 1)
        )

    def forward(self, vu, vt, vs):
        # n = vu.shape[0]
        # ind = np.random.randint(int(n * 0.05), int(n * 0.95))
        # vs_marg = torch.roll(vs, ind, 0)
        # vt_marg = torch.roll(vt, ind, 0)

        rand_perm = torch.randperm(vu.shape[0])
        vs_marg = vs[rand_perm, :]
        vt_marg = vt[rand_perm, :]

        # Estimate I(vu; vs, vt)
        t1 = self.T1(torch.cat((vu, vs, vt), dim=1)).mean()
        t1_marg = self.T1(torch.cat((vu, vs_marg, vt_marg), dim=1))

        # Estimate I(vu; vt)
        t2 = self.T2(torch.cat((vu, vt), dim=1)).mean()
        t2_marg = self.T2(torch.cat((vu, vt_marg), dim=1))

        if self.loss in ['mine']:
            second_term1, self.running_mean1 = ema_loss(
                t1_marg, self.running_mean1, self.alpha)
            second_term2, self.running_mean2 = ema_loss(
                t2_marg, self.running_mean2, self.alpha)
        elif self.loss in ['fdiv']:
            second_term1 = torch.exp(t1_marg - 1).mean()
            second_term2 = torch.exp(t2_marg - 1).mean()
        elif self.loss in ['mine_biased']:
            second_term1 = torch.logsumexp(
                t1_marg, 0) - math.log(t1_marg.shape[0])
            second_term2 = torch.logsumexp(
                t2_marg, 0) - math.log(t2_marg.shape[0])

        o1 = -t1 + second_term1
        o2 = -t2 + second_term2
        return o1 + o2, o1, o2

    def model_reset(self):
        for p in self.parameters():
            if p.dim() > 1:
                torch.nn.init.xavier_uniform_(p)


def train_tene(target, source, model, optimizer, m=1, tau=1, u=1,
               batch_size=16, num_epochs=100):

    # embedding
    target = target.reshape(target.shape[0], -1)
    source = source.reshape(source.shape[0], -1)
    vt = embedding(target, m, tau)
    vs = embedding(source, m, tau)
    vu = vt[u:, :]
    vt = vt[:-u, :]
    vs = vs[:-u, :]
    vu = torch.from_numpy(vu).float()
    vt = torch.from_numpy(vt).float()
    vs = torch.from_numpy(vs).float()

    for epoch in tqdm(range(num_epochs)):

        model.train()  # Set model to training mode

        # Iterate over data.
        for vu_b, vt_b, vs_b in batch(vu, vt, vs, bs=batch_size):

            # sample to GPU
            vu_b = vu_b.to(device)
            vt_b = vt_b.to(device)
            vs_b = vs_b.to(device)

            optimizer.zero_grad()

            # forward: track history if only in train
            with torch.set_grad_enabled(True):
                loss, _, _ = model(vu_b, vt_b, vs_b)
                loss.backward()
                optimizer.step()

    with torch.no_grad():
        vu = vu.to(device)
        vt = vt.to(device)
        vs = vs.to(device)
        loss, o1, o2 = model(vu, vt, vs)
    model.mi1 = -o1.item()
    model.mi2 = -o2.item()
    model.te = model.mi1 - model.mi2
    return model


if __name__ == "__main__":
    n = 10000
    c = np.linspace(0, 0.9, 20)
    m = 3
    tau = 1
    u = 4
    d_in = m
    batch_size = n // 50
    learning_rate = 1e-4
    model_ft = Tene(d_in, loss='mine', alpha=0.1,
                    dim_feedforward=100).to(device)
    opt_ft = opt.Adam(model_ft.parameters(), lr=learning_rate,
                      betas=(0.9, 0.99), weight_decay=0.001)

    txy = []
    tyx = []
    for i, c_ in enumerate(c):
        h = henonC(n, c_)
        x = h[:, 1]  # es causado por x2
        y = h[:, 0]

        model = train_tene(x, y, model=model_ft,
                           optimizer=opt_ft, m=m, tau=tau, u=u,
                           batch_size=batch_size, num_epochs=500)
        tyx.append(model.te)
        model_ft.model_reset()

        model = train_tene(y, x, model=model_ft,
                           optimizer=opt_ft, m=m, tau=tau, u=u,
                           batch_size=batch_size, num_epochs=500)
        txy.append(model.te)
        model_ft.model_reset()
        print(i / c.shape[0] * 100)
        # print(model.mi1, model.mi2, model.te)
    tyx = np.array(tyx)
    txy = np.array(txy)
    TE = np.array(tyx-txy)
    plt.plot(c, TE)
    plt.show()

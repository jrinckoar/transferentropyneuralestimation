#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''
Mine -- Tools
'''

import torch
import numpy as np


# Embedding -------------------------------------------------------------------
def embedding(x, m, tau):
    n, k = x.shape
    l = n - (m - 1) * tau
    X = np.zeros((l, k * m))
    rows = (np.arange(m)*(tau)) + np.arange(l).reshape(-1, 1)
    for i, xt in enumerate(x.T):
        X[:, i*m:(i + 1)*m] = xt[rows]
    return X


# batch -------------------------------------------------------------------
def tene_batch(vu, vt, vs, bs=1, shuffle='randperm'):

    if isinstance(vu, np.ndarray):
        vu = torch.from_numpy(vu).float()
    if isinstance(vt, np.ndarray):
        vt = torch.from_numpy(vt).float()
    if isinstance(vs, np.ndarray):
        vs = torch.from_numpy(vs).float()

    n = len(vu)

    # random circular shift
    if shuffle in ['randperm']:
        rand_perm = torch.randperm(n)
        vu = vu[rand_perm, :]
        vt = vt[rand_perm, :]
        vs = vs[rand_perm, :]
    elif shuffle in ['circ']:
        ind = np.random.randint(int(n * 0.05), int(n * 0.95))
        torch.roll(vu, ind, 0)
        torch.roll(vt, ind, 0)
        torch.roll(vs, ind, 0)

    batches = []
    for i in range(n // bs):
        vu_b = vu[i * bs: (i + 1) * bs, :]
        vt_b = vt[i * bs: (i + 1) * bs, :]
        vs_b = vs[i * bs: (i + 1) * bs, :]

        batches.append((vu_b, vt_b, vs_b))

    return batches


if __name__ == "__main__":

    n = 100
    m = 1
    tau = 1
    u = 0
    batch_size = 4
    target = np.random.randn(n, 1)
    source = np.random.randn(n, 1)

    V_b = tene_batch(target, source, m=m, tau=tau, u=u, batch_size=batch_size)
    print(V_b[0])
    print(target[:4])
    print(source[:4])

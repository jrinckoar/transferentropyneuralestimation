#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''
Dynamical systems
'''
from matplotlib import pyplot as plt
import numpy as np
# from mpl_toolkits.mplot3d import Axes3D
# from scipy.integrate import odeint


def henonC(n, c):
    '''Coupled Henon map X1->X2, X2->X3'''
    n0 = 10000
    n = n + n0
    x = np.zeros((n, 3))
    x[0:2, :] = np.random.rand(2, 3) * 0.1
    for i in range(2, n):
        x[i, 0] = 1.4 - x[i - 1, 0]**2 + 0.3 * x[i - 2, 0]

        x[i, 1] = 1.4 - c * x[i - 1, 0] * x[i - 1, 1] - \
            (1 - c) * x[i - 1, 1]**2 + 0.3 * x[i - 2, 1]

        x[i, 2] = 1.4 - c * x[i - 1, 1] * x[i - 1, 2] - \
            (1 - c) * x[i - 1, 2]**2 + 0.3 * x[i - 2, 2]

    return x[n0:, :]


def BivariateGaussian(n_, mu_=0, rho_=0):

    Mu = np.ones(2) * mu_
    Cov = np.ones((2, 2))
    Cov[0, 1] = rho_
    Cov[1, 0] = rho_
    X = np.random.multivariate_normal(mean=Mu, cov=Cov, size=(n_, 1))
    return X[:, :, 0], X[:, :, 1]


if __name__ == "__main__":
    n = 3000
    rho = -0.5
    mu = 0
    x, z = BivariateGaussian(n, mu, rho)
    plt.plot(x, z, '.')
    plt.show()
